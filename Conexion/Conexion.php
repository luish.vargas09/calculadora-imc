<?php 

class Conexion {
        //Atributos
        private $host = 'localhost';
        private $port = 3308;
        private $user = 'root';
        private $password = '';
        private $db = 'db_imc';
        protected $conex;

        function __construct()
        {
            $dataDB = "mysql:host=$this->host; port=$this->port; dbname=$this->db";
            try {
                $this->conex = new PDO($dataDB, $this->user, $this->password);
                $this->conex->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                echo "La base de datos ha sido conectada exitosamente";
            } catch (Exception $e) {
                $this->conex = "Ha ocurrido un error en la conexión a la BD";
                echo $this->conex;
            }

        }

        
    }


?>
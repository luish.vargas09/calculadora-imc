CREATE TABLE usuario (
    id_usuario int(11) PRIMARY KEY AUTO_INCREMENT,
    nombre varchar(30) NOT NULL, 
    peso_usuario double(5,2) NOT NULL,
    altura_usuario double(5,2) NOT NULL,
    imc_usuario double(4,2) NOT NULL,
    estado_usuario varchar(255) NOT NULL,
    fecha_registro_usuario TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP()
)ENGINE=InnoDB;


INSERT INTO usuario (id_usuario, nombre, peso_usuario, altura_usuario, imc_usuario, estado_usuario, fecha_registro_usuario) 
VALUES 
(1, "Paco" ,90.00, 179.00, 30.31, 'Peso en Alto (Sobrepeso)', '2021-06-21 17:41:49'),
(2, "Noe" ,100.00, 185.00, 35.31, 'Peso en Alto (Sobrepeso)', '2021-06-21 17:50:13'),
(3, "Villa" ,70.00, 170.00, 24.31, 'Peso en Alto (Sobrepeso)', '2021-06-21 17:59:35')




<?php 

    require_once('Conexion/Conexion.php');

    class Registro extends Conexion {

        public function __construct() {
            parent::__construct();
        }

        public function insertarDatos(float $peso, float $altura, float $imc, string $estado) 
        {
            $query = 'INSERT INTO usuario(nombre, peso_usuario, altura_usuario, imc_usuario, estado_usuario) VALUES (?, ?, ?, ?, ?)';
            $prepare = $this->conex->prepare($query);
            $arrData = [$peso, $altura, $imc, $estado];
            $result = $prepare->execute($arrData);
            $id = $this->conex->lastInsertId();

            return $id;
        }

        public function obtenerRegistros()
        {
            $query = 'SELECT * FROM usuario ORDER BY imc_usuario DESC';
            $result = $this->conex->query($query);
            $rows = $result->fetchall(PDO::FETCH_ASSOC);

            if (count($rows) > 0 ) {
                return $rows;
            }else {
                return "Aún no hay registros";
            }
        }
        

    }

?>